import java.util.Arrays;

public class Main {

    public static int[] mergeTwoArraySort(int[] A, int[] B, int m, int n) {
        int[] R = new int[m + n];
        int index = 0;

        // merge A to R
        for (int i = 0; i < m + n; i++)
            if (A[i] != Integer.MIN_VALUE) {
                R[index] = A[i];
                index++;
            }

        // merge B to R
        for (int i = 0; i < n; i++) {
            R[index] = B[i];
            index++;
        }

        // Sort R
        Arrays.sort(R);
        return R;
    }

    public static int[] mergeTwoArray(int[] A, int[] B, int m, int n) {
        int[] R = new int[m + n];
        int i = 0, j = 0, k = 0;

        while (i < m + n && j < m + n) {
            if (A[j] <= B[k]) {
                R[i] = A[j];
                j++;
            }
            else {
                R[i] = B[k];
                k++;
            }
            if (R[i] != Integer.MIN_VALUE)
                i++;
        }

        // put all element from B to R
        if (k < n) {
            for(int _i = k; _i < n; _i++) {
                R[i] = B[_i];
                i++;
            }
        }
        return R;
    }


    public static void main(String[] args) {
        int m = 5, n = 3;
        int[] A = {1, Integer.MIN_VALUE, 5, 7, Integer.MIN_VALUE, 10, Integer.MIN_VALUE, 20};
        int[] B = {8, 13, 25};
//        int[] R = mergeTwoArray(A, B, m, n);
        int[] R = mergeTwoArray(A, B, m, n);
        for (int value : R)
            System.out.print(value + ", ");
    }
}
